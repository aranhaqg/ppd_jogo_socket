package br.edu.ifce.ppd.jogos.jogo_1.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Board;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.BoardPosition;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.TileEvent;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Utils;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class ClientHandler extends Thread {
	private Socket connection;
	//Bus handle only server-side events locally. All events between server and clients are done with Sockets.
	private EventBus bus;
	private BufferedReader in;
	private PrintWriter out;
	private Server server;
	private Board board;
	private int clientId;
	private boolean loseTurn;

	public ClientHandler(Socket connection, EventBus channel, Server server, int clientId) {
		this.connection = connection;
		this.bus = channel;
		this.server = server;
		this.board = new Board();
		this.clientId=clientId;
		this.loseTurn=false;
		try {
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			out = new PrintWriter(connection.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Subscribe
	public void receiveMessage(String message) {
		if (out != null) {
			//game events
			int clientIdEventFrom = Integer.parseInt(message.substring(0,1));
			String event = message.substring(1, 5);

			switch (event) {
			case "CHAT":
				String chatMessage = message.substring(5);
				System.out.println("PLAYER " + clientIdEventFrom + " CHAT MESSAGE ==" + chatMessage);
				
				break;
			case "LOGG":
				String loggMessage = message.substring(5);
				System.out.println("PLAYER " + clientIdEventFrom + " LOGG MESSAGE ==" + message);
				bus.post(clientIdEventFrom+"WAIT");
				break;
			case "STDO":
				String startDoneMessage = "Começando o jogo...\n";
				System.out.println("PLAYER " + clientIdEventFrom + " START DONE MESSAGE ==" + message);
				out.println(clientIdEventFrom+"STDO"+startDoneMessage);
				break;
			case "OVER":
				String gameOverMessage = message.substring(5);
				System.out.println("PLAYER " + clientIdEventFrom + " CHAT MESSAGE ==" + gameOverMessage);
				break;
			case "UPMO":
				String UpdteMoveMessage = message.substring(5);
				System.out.println("PLAYER " + clientIdEventFrom + " LOGG MESSAGE ==" + UpdteMoveMessage);
				break;
			case "ROLL":
				//Only roll one client per time
				if(clientId==clientIdEventFrom){
					System.out.println("PLAYER "+clientIdEventFrom + " ROLL ");
										
					if(loseTurn){
						System.out.println("LOSE TURN CASE");
						bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" não jogará este turno. ");
						out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" não jogará este turno. ");
						//TODO: testar esse caso
						bus.post(clientIdEventFrom+"TURN");
						loseTurn=false;
						break;
					}
					
					int result = Utils.randInt(1, 6);
					
					//Bus.post method is executed at the eof. 
					bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" tirou "+result+" nos dados");
					out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" tirou "+result+" nos dados");
					
					int oldPlayerPosition = Integer.parseInt(message.substring(5));
					int newPlayerPosition = result+oldPlayerPosition; 
					
					//End of the Board
					if(newPlayerPosition>=34){
						newPlayerPosition=34;
					}
					
					TileEvent tileEvent = board.getBoardPositions()[newPlayerPosition].getTileEvent();
					System.out.println("Player "+clientIdEventFrom+" ROLL - Tile Event = "+tileEvent.toString());

					/*//testing purposes
					if(oldPlayerPosition>=8){
						tileEvent=TileEvent.ROLL_AGAIN;
					}*/
					switch (tileEvent) {
					case END:
						//End of game. Inform the winner.
						bus.post(clientIdEventFrom+"OVER");
						break;
					case GO_BACK:
						//reply the new position 
						bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" caiu na casa de voltar duas casas.");
						out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" caiu na casa de voltar duas casas.");
						newPlayerPosition= newPlayerPosition -2;
						out.println(clientIdEventFrom+"MOVE"+newPlayerPosition);
						break;
					case GO_FORWARD:
						bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" caiu na casa de avançar duas casas.");
						out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" caiu na casa de avançar duas casas.");
						newPlayerPosition= newPlayerPosition +2;
						out.println(clientIdEventFrom+"MOVE"+newPlayerPosition);
						break;
					case LOSE_TURN:
						//control the lost turn
						bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" caiu na casa de perder um turno.");
						out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" caiu na casa de perder um turno.");
						out.println(clientIdEventFrom+"LOST"+newPlayerPosition);
						loseTurn=true;
						System.out.println("Client "+ clientId + " Lose turn: " + loseTurn);
						break;
					case NOP:
						//reply with MOVE only
						out.println(clientIdEventFrom+"MOVE"+newPlayerPosition);
						break;
					case ROLL_AGAIN:
						//reply with REROLL 
						bus.post(clientIdEventFrom+"BROA"+"Jogador "+clientIdEventFrom+" caiu na casa de re-rolar os dados.");
						out.println(clientIdEventFrom+"LOGG"+"Jogador "+clientIdEventFrom+" caiu na casa re-rolar os dados.");
						
						bus.post(clientIdEventFrom+"RERO"+newPlayerPosition);
						break;
					case START:
						break;
					default:
						break;
					}
				}
				break;
			case "STRT":
				// start event from player; reply with STOK to clients
				out.println(clientIdEventFrom+"STOK");
				break;
			case "DISC":
				// client disconnected
				out.println(clientIdEventFrom+"LOGG"+"Player "+clientIdEventFrom+ " has disconnected.");

				System.out.println("Client "+ clientIdEventFrom + " has disconnected.");
				if(server.getClientCount()>0) {
					server.setClientCount(server.getClientCount() - 1);
				}
				System.out.println("Client count: " + server.getClientCount());
				break;	
			default:
				break;
			}
			out.println(message);
		}
	}

	@Override
	public void run() {
		try {
			String input;
			//Read remote socket message and post to local ClientsHandlers. 
			//Connection between clients (MainWindow) and servers are done only with sockets.
			while ((input = in.readLine()) != null) {
				bus.post(input);
			}
		} catch (SocketException e) {
			System.out.println("Socket finalizado.");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		closeConnection();
	}

	private void closeConnection(){
		//reached eof
		bus.unregister(this);
		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		in = null;
		out = null;
	}
}
