package br.edu.ifce.ppd.jogos.jogo_1.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

import com.google.common.eventbus.EventBus;

public class Server {
	private int clientCount;
	private EventBus channel;
	private PrintStream ps;
	
	public static void main(String[] args) {
		Server server = new Server(0);	
		server.channel = new EventBus();
		
		try {
			server.socket = new ServerSocket(4444);
			System.out.println("Server iniciado!");

			while (true) {
				Socket connection = server.socket.accept();
				server.ps = new PrintStream(connection.getOutputStream());
				if(server.getClientCount()>=2){
					System.out.println("Servidor cheio");
					server.ps.println("Servidor cheio");
					connection.close();
				}else{
					//Send id to client
					server.ps.println(server.getClientCount());
					System.out.println("Novo cliente conectado!");
					ClientHandler newUser = new ClientHandler(connection, server.getChannel(), server,server.getClientCount());
					
					server.setClientCount(server.getClientCount() + 1);
					server.getChannel().register(newUser);
					newUser.start();
					
					//Posting Start game event to Server-Side events handlers (ClientHandler). 
					//Events between clients and server are all done with SOCKET.
					if(server.getClientCount()==2) {
						server.getChannel().post("0STRT");
					}
					System.out.println("Client count: " + server.getClientCount());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Server(int clientCount) {
		super();
		this.clientCount = clientCount;
	}

	public int getClientCount() {
		return clientCount;
	}

	public void setClientCount(int clientCount) {
		this.clientCount = clientCount;
	}
	public EventBus getChannel() {
		return channel;
	}

	public void setChannel(EventBus channel) {
		this.channel = channel;
	}

	public ServerSocket getSocket() {
		return socket;
	}

	public void setSocket(ServerSocket socket) {
		this.socket = socket;
	}

	public PrintStream getPs() {
		return ps;
	}

	public void setPs(PrintStream ps) {
		this.ps = ps;
	}
	
	private ServerSocket socket;
}
