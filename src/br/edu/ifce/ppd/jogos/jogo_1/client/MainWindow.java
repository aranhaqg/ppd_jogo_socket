package br.edu.ifce.ppd.jogos.jogo_1.client;

import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JLayeredPane;

import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Board;

public class MainWindow extends Thread{
	private static final String PLAYER0_IMG_PATH = "assets/playerTokenBlueSmall.png";
	private static final String PLAYER1_IMG_PATH = "assets/playerTokenRedSmall.png";
	private JFrame frame;
	private JTextField textField;
	private JTextArea textChatHistory ;
	private final String IMG_PATH = "assets/tabuleiro.png";
	private Board board;
	private int playerPosition =0;
	private ImageIcon boardIcon;
	private BufferedImage boardImg;
	private JLayeredPane boardPanel;
	private BufferedImage player0Img;
	private ImageIcon player0Icon;
	private BufferedImage player1Img;
	private ImageIcon player1Icon;
	private JLabel player0Label;
	private JLabel player1Label;
	private JPanel chatPanel;
	private JButton btnStart;
	private JPanel chatHistoryPanel;
	private JButton btnSubmit;
	private JLabel boardLabel;
	static int clientId;
	static boolean loseTurn=false;
	static Socket  server = null;
	static PrintStream ps = null;
	static BufferedReader br = null;
	private JButton btnRoll;
	private boolean reRoll=false;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Connect to server through socket
					server = new Socket("127.0.0.1",4444);
					System.out.println("Conectado ao server pela porta 4444.");

					ps = new PrintStream(server.getOutputStream());
					br = new BufferedReader(new InputStreamReader(server.getInputStream()));
					String serverMessage = br.readLine();
					try{
						clientId = Integer.parseInt(serverMessage);
						MainWindow window = new MainWindow();
						window.frame.setVisible(true);
						window.updateChatHistory("Conectado ao server.\nVocê é o jogador " +clientId+". \n");
						window.start();
					}catch(NumberFormatException e){
						System.out.println(serverMessage);
						MainWindow window = new MainWindow();
						window.frame.setVisible(true);
						window.updateChatHistory(serverMessage+ ".\nTente conectar novamente depois.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void updateChatHistory(String chatMessage) {
		getTextChatHistory().append(chatMessage);
		getTextChatHistory().setCaretPosition(getTextChatHistory().getDocument().getLength());

	}
	public MainWindow() {
		board = new Board(); 
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		chatPanel = new JPanel();
		frame.getContentPane().add(chatPanel, BorderLayout.SOUTH);

		textField = new JTextField();
		chatPanel.add(textField);
		textField.setColumns(40);

		btnSubmit = new JButton("Submit");
		btnStart = new JButton("Start");
		btnRoll = new JButton("Roll");
		btnSubmit.addActionListener(new ButtonSubmitClickListener());
		btnStart.addActionListener(new ButtonStartClickListener());
		btnRoll.addActionListener(new ButtonRollActionListener());
		chatPanel.add(btnSubmit);
		chatPanel.add(btnRoll);
		chatPanel.add(btnStart);
		btnStart.setEnabled(false);
		btnRoll.setEnabled(false);

		chatHistoryPanel = new JPanel();
		frame.getContentPane().add(chatHistoryPanel, BorderLayout.WEST);

		textChatHistory = new JTextArea();
		textChatHistory.setRows(34);
		textChatHistory.setColumns(20);
		textChatHistory.setEditable(false);
		JScrollPane scroll = new JScrollPane(textChatHistory);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatHistoryPanel.add(scroll);

		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt){
				desconectar(evt);
			}
		});

		boardPanel = new JLayeredPane();
		frame.getContentPane().add(boardPanel, BorderLayout.CENTER);
		boardPanel.setLayout(null);

		try {
			boardImg = ImageIO.read(new File(IMG_PATH));
			boardIcon = new ImageIcon(boardImg);

			player0Img = ImageIO.read(new File(PLAYER0_IMG_PATH));
			player0Icon = new ImageIcon(player0Img);

			player1Img = ImageIO.read(new File(PLAYER1_IMG_PATH));
			player1Icon = new ImageIcon(player1Img);

		} catch (IOException e) {
			e.printStackTrace();
		}

		boardLabel = new JLabel(boardIcon);
		player0Label = new JLabel(player0Icon);
		player1Label = new JLabel(player1Icon);

		boardPanel.add(boardLabel, 1);
		boardLabel.setBounds(0, 0, 535, 348);
	}

	protected void desconectar(WindowEvent evt) {
		ps.println(clientId+"DISC"+evt);
		frame.dispose();
	}

	@Override
	public void run() {
		while(true) {
			String message;
			try {
				message = br.readLine();
				int clientIdEventFrom = Integer.parseInt(message.substring(0,1));
				String event = message.substring(1, 5);

				switch (event) {
				case "CHAT":
					String chatMessage = message.substring(5);
					//ps.println(clientId+"CHATteste");
					System.out.println("PLAYER " + clientId + " CHAT MESSAGE ==" + chatMessage);
					updateChatHistory("Player "+ clientIdEventFrom + ": "+chatMessage+"\n");
					break;
				case "LOGG":
					String logMessage = message.substring(5);
					System.out.println("PLAYER " + clientIdEventFrom + " LOGG MESSAGE ==" + logMessage);
					
					btnStart.setEnabled(false);
					updateChatHistory(logMessage+"\n");
					break;
				case "BROA":
					String broadcastMessage = message.substring(5);
					System.out.println("PLAYER " + clientIdEventFrom + " BROADCAST MESSAGE ==" + broadcastMessage);
					if(clientIdEventFrom!=clientId){
						updateChatHistory(broadcastMessage+"\n");
					}
					break;
				case "MOVE":
					int newPosition = Integer.parseInt(message.substring(5));
					localMoveToken(clientIdEventFrom, newPosition);
					System.out.println(clientIdEventFrom+ " MOVE Position "+newPosition);
					ps.println(clientId+"WAIT");
					break;
				case "UPMO":
					int newUpPosition = Integer.parseInt(message.substring(5));
					updateTokenMove(clientIdEventFrom, newUpPosition);

					ps.println(clientId+"WAIT");
					if(reRoll){
						btnRoll.setEnabled(false);
						reRoll=false;
					}

					break;
				case "RERO":
					int newReroPosition = Integer.parseInt(message.substring(5));
					localMoveToken(clientIdEventFrom, newReroPosition);
					
					if(clientIdEventFrom==clientId){
						btnRoll.setEnabled(true);
						updateChatHistory("Re-role os dados.\n");
					}else{
						btnRoll.setEnabled(false);
						updateChatHistory("O oponente irá re-rolar os dados.\n");
						reRoll=true;
					}
						
					//ps.println(clientIdEventFrom+"ROLL"+newReroPosition);
					break;
				case "LOST":
					int newLostTurnPosition = Integer.parseInt(message.substring(5));
					localMoveToken(clientIdEventFrom, newLostTurnPosition);
					updateChatHistory("Você ficará um turno sem jogar.\n");
					btnRoll.setEnabled(false);
					break;
				case "TURN":
					if(clientIdEventFrom==clientId){
						btnRoll.setEnabled(false);

					}else{
						btnRoll.setEnabled(true);
					}
					break;
				case "STOK":
					System.out.println("STOK event received");
					updateChatHistory("Decidam através do chat quem \ndeve começar a jogar. Somente o primeiro\n a jogar deve apertar o botão de START."+"\n");

					boardPanel.add(player0Label, 0);
					boardPanel.add(player1Label, 0);

					player0Label.setBounds(board.getBoardPositions()[0].getImage_x(), 
							board.getBoardPositions()[0].getImage_y(), 30, 42);
					player1Label.setBounds(board.getBoardPositions()[0].getImage_x(), 
							board.getBoardPositions()[0].getImage_y()+Board.OFFSET_PLAYER_1_y, 30, 42);


					btnStart.setEnabled(true);
					break;
				case "OVER":
					localMoveToken(clientIdEventFrom, 34);
					btnRoll.setEnabled(false);
					if(clientIdEventFrom==clientId){
						updateChatHistory("Parabéns! Você ganhou o jogo.\n");
					}else {
						updateChatHistory("Sinto muito. Você perdeu.\n");
					}
					break;
				case "WAIT":
					System.out.println(clientIdEventFrom+ " sent WAIT.");
					break;
				default:
					break;
				}
				Thread.sleep(1000);
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void localMoveToken(int clientIdEventFrom, int newPosition) {
		
		if(clientIdEventFrom==clientId){
			player0Label.setBounds(board.getBoardPositions()[newPosition].getImage_x(), 
				board.getBoardPositions()[newPosition].getImage_y(), 30, 42);
			updateChatHistory("Você moveu para a casa "+newPosition+".\n");

			playerPosition = newPosition;
			ps.println(clientIdEventFrom+"UPMO"+newPosition);
		}
		


	}

	private void updateTokenMove(int clientIdEventFrom, int newUpPosition){
		if(clientIdEventFrom!=clientId){
			player1Label.setBounds(board.getBoardPositions()[newUpPosition].getImage_x(), 
					board.getBoardPositions()[newUpPosition].getImage_y()+Board.OFFSET_PLAYER_1_y, 30, 42);
			updateChatHistory("Oponente moveu para a casa "+newUpPosition+".\n");
			btnRoll.setEnabled(true);
		}
		
	}

	private class ButtonSubmitClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String newMessage = getTextField().getText();
			getTextField().setText("");
			ps.println(clientId+"CHAT"+newMessage);
		}		
	}
	private class ButtonStartClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ps.println(clientId+"LOGG"+"Começando jogo..\n");
			btnStart.setEnabled(false);
			btnRoll.setEnabled(true);
		}
	}

	private class ButtonRollActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			ps.println(clientId+"ROLL"+playerPosition);
			btnRoll.setEnabled(false);
		}
	}

	public JTextArea getTextChatHistory() {
		return textChatHistory;
	}

	public void setTextChatHistory(JTextArea textChatHistory) {
		this.textChatHistory = textChatHistory;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}
}
