package br.edu.ifce.ppd.jogos.jogo_1.game_logic;

public enum TileEvent {
	NOP,
	START,
	GO_BACK,
	LOSE_TURN,
	ROLL_AGAIN,
	GO_FORWARD,
	END;
}
