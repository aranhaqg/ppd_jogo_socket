package br.edu.ifce.ppd.jogos.jogo_1.game_logic;

public class Board {
	public static final int OFFSET_PLAYER_1_y = 10;
	public static final int OFFSET_TILE_x = 70;
	public static final int OFFSET_TILE_y = 70;
	public static final int OFFSET_START_x = 45;
	public static final int OFFSET_START_y = 10;
	private BoardPosition[] boardPositions = new BoardPosition[35];
	
	public Board() {
		boardPositions[0] = new BoardPosition(TileEvent.START, OFFSET_START_x, OFFSET_START_y);
		boardPositions[1] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + OFFSET_TILE_x, OFFSET_START_y);
		boardPositions[2] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 2*OFFSET_TILE_x, OFFSET_START_y);
		boardPositions[3] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 3*OFFSET_TILE_x, OFFSET_START_y);
		boardPositions[4] = new BoardPosition(TileEvent.GO_BACK, OFFSET_START_x + 4*OFFSET_TILE_x, OFFSET_START_y);
		boardPositions[5] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 5*OFFSET_TILE_x, OFFSET_START_y);
		boardPositions[6] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 6*OFFSET_TILE_x-10, OFFSET_START_y);

		boardPositions[7] = new BoardPosition(TileEvent.LOSE_TURN, OFFSET_START_x+6*OFFSET_TILE_x-10, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[8] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 5*OFFSET_TILE_x, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[9] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 4*OFFSET_TILE_x, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[10] = new BoardPosition(TileEvent.ROLL_AGAIN, OFFSET_START_x + 3*OFFSET_TILE_x, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[11] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 2*OFFSET_TILE_x, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[12] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + OFFSET_TILE_x, OFFSET_START_y + OFFSET_TILE_y);
		boardPositions[13] = new BoardPosition(TileEvent.NOP, OFFSET_START_x, OFFSET_START_y + OFFSET_TILE_y);

		boardPositions[14] = new BoardPosition(TileEvent.NOP, OFFSET_START_x+10, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[15] = new BoardPosition(TileEvent.GO_FORWARD, OFFSET_START_x + OFFSET_TILE_x, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[16] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 2*OFFSET_TILE_x, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[17] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 3*OFFSET_TILE_x, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[18] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 4*OFFSET_TILE_x, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[19] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 5*OFFSET_TILE_x, OFFSET_START_y + 2*OFFSET_TILE_y);
		boardPositions[20] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 6*OFFSET_TILE_x-10, OFFSET_START_y + 2*OFFSET_TILE_y);

		boardPositions[21] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 6*OFFSET_TILE_x-10, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[22] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 5*OFFSET_TILE_x, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[23] = new BoardPosition(TileEvent.LOSE_TURN, OFFSET_START_x + 4*OFFSET_TILE_x, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[24] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 3*OFFSET_TILE_x, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[25] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 2*OFFSET_TILE_x, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[26] = new BoardPosition(TileEvent.ROLL_AGAIN, OFFSET_START_x + OFFSET_TILE_x, OFFSET_START_y + 3*OFFSET_TILE_y);
		boardPositions[27] = new BoardPosition(TileEvent.NOP, OFFSET_START_x+10, OFFSET_START_y + 3*OFFSET_TILE_y);

		boardPositions[28] = new BoardPosition(TileEvent.NOP, OFFSET_START_x+10, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[29] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + OFFSET_TILE_x, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[30] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 2*OFFSET_TILE_x, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[31] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 3*OFFSET_TILE_x, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[32] = new BoardPosition(TileEvent.GO_BACK, OFFSET_START_x + 4*OFFSET_TILE_x, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[33] = new BoardPosition(TileEvent.NOP, OFFSET_START_x + 5*OFFSET_TILE_x, OFFSET_START_y + 4*OFFSET_TILE_y);
		boardPositions[34] = new BoardPosition(TileEvent.END, OFFSET_START_x + 6*OFFSET_TILE_x-10, OFFSET_START_y + 4*OFFSET_TILE_y);
	}

	public BoardPosition[] getBoardPositions() {
		return boardPositions;
	}
}
